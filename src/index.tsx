import { hydrate } from 'react-dom';
import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { loadableReady } from '@loadable/component';
import { Provider } from 'react-redux';

import configureStore from '@store';
import GlobalStyle from '@styles/GlobalStyle';
import App from './App';

const store = configureStore();

// loadableReady
// SSR 전용 함수로, 이 함수를 사용하면 모든 자바스크립트 파일을 동시에 받아온다.
// 따라서 코드 분할(Code-splitting)되어 있는 컴포넌트들을
// 정상적으로 렌더링할 수 있게 준비할 수 있다.
loadableReady(() => {
  const rootElement = document.getElementById('root');
  // hydrate
  // SSR에서 render 함수 대신에 사용할 수 있는 함수로,
  // 기존 SSR 결과물이 이미 로드되어 있을 경우 새로 렌더링하지 않고
  // 기존에 렌더링 되어있는 UI에 Javascript 이벤트만 연동해서
  // 초기 구동시 필요한 리소스를 최소화한다.
  hydrate(
    <Provider store={store}>
      <BrowserRouter>
        <>
          <GlobalStyle />
          <App />
        </>
      </BrowserRouter>
    </Provider>,
    rootElement,
  );
});

if (module.hot) {
  module.hot.accept();
}
