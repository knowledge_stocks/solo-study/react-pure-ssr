import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import loadable from '@loadable/component';

// lodable
// 컴포넌트들을 코드분할(Code-splitting)해주는 import 함수
// 보통 Route 단위로 나누는 것이 일반적이지만
// 어떻게 동작하는지 테스트를 위해, Header와 Footer에도 적용되어 있다.
const Header = loadable(() => import(/* webpackChunkName: "Header" */ './components/Header'));
const Footer = loadable(() => import(/* webpackChunkName: "Footer" */ './components/Footer'));
const Home = loadable(() => import(/* webpackChunkName: "Home" */ './pages/Home'));
const News = loadable(() => import(/* webpackChunkName: "News" */ './pages/News'));

export default function App() {
  return (
    <div>
      <Helmet>
        <title>App</title>
      </Helmet>
      <Route path="/" render={() => <Header />} />
      <Switch>
        <Route exact path="/" render={() => <Home />} />
        <Route path="/news" render={() => <News />} />
      </Switch>
      <Footer />
    </div>
  );
}
